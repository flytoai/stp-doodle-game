import torch
import torch.nn as nn
import geffnet

from FlytoAI.models import FlytoaiMobilenetV2


def squeeze_weights(m):
    m.weight.data = m.weight.data.sum(dim=1)[:, None]
    m.in_channels = 1

class FlytoaiClassifier(nn.Module):
    "EfficentNet models wrapper."

    def __init__(self, num_features):
        super().__init__()
        self.base_model = geffnet.efficientnet_b0(pretrained=False)
        self.base_model.conv_stem.apply(squeeze_weights)
        embed_size = self.base_model.classifier.in_features

        self.base_model.classifier = nn.Identity()

        self.classifier = nn.Linear(in_features=embed_size, out_features=num_features)

    def forward(self, input):
        img_code = self.base_model(input)
        outs = self.classifier(img_code)

        return outs


def load_v_2(name):
    model = FlytoaiMobilenetV2(340, 3)
    checkpoint = torch.load(f'Backend/backend/backend/data_science/models/{name}', map_location='cpu')
    model.load_state_dict(checkpoint['model'])
    model.eval()

    return model


def load_v_1(name):
    model = FlytoaiClassifier(340)

    #optimizer = TheOptimizerClass(*args, **kwargs)

    checkpoint = torch.load(f'Backend/backend/data_science/models/{name}', map_location='cpu')

    checkpoint = torch.load(f'Backend/backend/backend/data_science/models/{name}', map_location='cpu')

    model.load_state_dict(checkpoint['model'])
    model.eval()

    return model
    #model.eval()
    # - or -
    #model.train()