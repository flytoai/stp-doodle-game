from django.apps import AppConfig


class PictureRecognitionConfig(AppConfig):
    name = 'picture_recognition'
