from django.db import models

# Create your models here.

class Player(models.Model):

    user_name = models.TextField('Name', default="Untitled", primary_key=True)
    score = models.IntegerField('Score', default=0)

    def __str__(self):
        return self.user_name + "-" + str(self.score) + "|"
