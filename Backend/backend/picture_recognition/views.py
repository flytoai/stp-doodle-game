from django.shortcuts import render
from django.conf import settings
from django.http import HttpResponse


from ast import literal_eval
from random import shuffle
from random import choice

from .models import Player
from .misc.preprocess import chanelize

import torch
import numpy as np

dictionary = settings.DICTIONARY
words = list(dictionary.values())
model = settings.MODEL
task = settings.TASK

def main(request):
    return render(request, 'index_stp.html')

def generate_task(request):

    is_guessed = request.body
    is_guessed = int(literal_eval(is_guessed.decode("UTF-8"))['data'])
    task = choice(words)

    return HttpResponse(f'<li>{task}</li>')

def save_get_leaders(request):

    data = literal_eval(request.body.decode("UTF-8"))

    print(data)
    name = data['name']
    score = int(data['score'])
    print(name)
    objects = Player.objects.all().filter(user_name=name)

    if len(objects) == 0:
        object = Player.objects.create(user_name=name)
    else:
        object = objects.get(user_name=name)

    object.score = max(object.score, score)
    object.save()
    objects = Player.objects.all().order_by('-score')[0: 10]
    print(objects)
    return HttpResponse(objects)

def save_image(request):

    sequences = request.body
    sequences = literal_eval(sequences.decode("UTF-8"))['data']
    pic_chanelized = torch.unsqueeze(torch.from_numpy(chanelize(sequences, size=112)), 0)

    output = model(pic_chanelized)
    _, pred = output.topk(3, 1, True, True)
    shuffle(pred)

    decoded = list(map(lambda encoded: dictionary[encoded], np.array(pred).ravel()))
    variants = list(map(lambda decod: f'<li>{decod}</li>', decoded))
    results = ' '.join(variants)

    return HttpResponse(results)