from django.urls import path, re_path, reverse
from .views import main, save_image, generate_task, save_get_leaders

urlpatterns = [

    path('',  main),
    re_path(r'save_image/*', save_image, name='save'),
    re_path(r'generate_task/*', generate_task, name='task'),
    re_path(r'save_leader/*', save_get_leaders, name='save_leader'),

]