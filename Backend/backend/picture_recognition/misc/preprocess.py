import numpy as np
import copy
from cv2 import resize, line

def border_detection(FullMatrix): #takes matrix (with zeros and ones) and returns matrix of borders
    border = np.zeros(FullMatrix.shape)
    border[:,-1] = FullMatrix[:,-1]
    border[-1,:] = FullMatrix[-1,:]
    border[:,0] = FullMatrix[:,0]
    border[0,:] = FullMatrix[0,:]
    for i in range(1,FullMatrix.shape[0] - 1):
        for j in range(1, FullMatrix.shape[1] - 1):
            if (FullMatrix[i-1,j] == 0 or FullMatrix[i+1,j] == 0 \
            or FullMatrix[i,j-1] == 0 or FullMatrix[i,j+1] == 0) and FullMatrix[i,j] != 0:
                border[i,j] = 1
    return border

def podgonka(image_1):
    image = copy.copy(image_1)
    for i in range(image.shape[0]):
        for j in range(image.shape[1]):
            if image[i,j] < 200:
                image[i,j] = 1
            else:
                image[i,j] = 0
    return image

def chanelize(raw_strokes, size=256, lw=2):
    BASE_SIZE = 256
    # roztyazhka

    img = np.zeros((BASE_SIZE, BASE_SIZE, 3), np.uint8)
    for t, stroke in enumerate(raw_strokes):
        points_count = len(stroke[0]) - 1
        if points_count > 0:
            grad = 255 // points_count
        for i in range(len(stroke[0]) - 1):
            _ = line(
                img,
                (stroke[0][i], stroke[1][i]),
                (stroke[0][i + 1], stroke[1][i + 1]),
                (255, 255 - min(t, 10) * 13, max(255 - grad * i, 20)),
                lw,
            )
    # allign to left top corner
    x_to_cut, y_to_cut = np.min(np.argwhere(img[:, :, 0] != 0), axis=0)
    cut_img = img[x_to_cut:, y_to_cut:, :]
    img = np.zeros(img.shape)
    img[:cut_img.shape[0], :cut_img.shape[1], :] = cut_img

    if size != BASE_SIZE:
        img = resize(img, (size, size))

    return (img.transpose(2, 0, 1) / 255).astype("float32")