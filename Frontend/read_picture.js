$(document).ready(
	
	function(){

		const csrftoken = getCookie('csrftoken')
		var timer_function

		var canvas_raw = document.getElementById('canvas')
		var leaderboard = document.getElementById('leaderboard')
		var timer_display = document.querySelector('#time')
		var score_display = document.querySelector('#score')
        var name_display = document.querySelector('#name')
		const step = 1000
		var current_score = -1

		var guess = 0
		var counter = -1
		var game_on = false
		
		var last_said = ''
		
		var first = true
		var canvas = $('#canvas')
		var moving = false
		
		var prev_x = 0
		var prev_y = 0
		
		var curr_x = 0
		var curr_y = 0

		var width = canvas_raw.width
		var height = canvas_raw.height
		var ctx = canvas_raw.getContext("2d")

		var clear_button = $('#clear')
		var start_stop_button = $('#start-stop')
		var skip_button = $('#skip')
		
		var bounds = canvas_raw.getBoundingClientRect()

		var coords = []
		var strokes = []
		
		ctx.fillStyle = 'black'

		function startTimer(duration, display) {
			var timer = duration, minutes, seconds;
			timer_function = setInterval(function () {
				minutes = parseInt(timer / 60, 10);
				seconds = parseInt(timer % 60, 10);

				minutes = minutes < 10 ? "0" + minutes : minutes;
				seconds = seconds < 10 ? "0" + seconds : seconds;

				display.textContent = minutes + ":" + seconds;
				if (--timer < 0) {
					start_stop_game()
				}
			}, 1000);
		}

		function stopTimer(display){
			display.textContent = "01" + ":" + "00";
			console.log("timer stopper")
			clearInterval(timer_function)
		}

		function getCookie(name) {
			
			let cookieValue = null;
			
			if (document.cookie && document.cookie !== '') {
				const cookies = document.cookie.split(';');
				for (let i = 0; i < cookies.length; i++) {
					const cookie = cookies[i].trim();
					// Does this cookie string begin with the name we want?
					if (cookie.substring(0, name.length + 1) === (name + '=')) {
						cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
						break;
					}
				}
			}
			return cookieValue;
			}
		
		
		draw = function(event, isStart){
			
			var click_x = event.pageX
			var click_y = event.pageY
			var offset_x = canvas.offset().left
			var offset_y = canvas.offset().top
			
			counter = (counter + 1) % step
			
			if (isStart){
				
				prev_x = ((click_x - offset_x) / bounds.width) * width
				prev_y = ((click_y - offset_y) / bounds.height) * height
				
			}
			
			else{
				
				prev_x = curr_x
				prev_y = curr_y
				
			}
			
			curr_x = ((click_x - offset_x) / bounds.width) * width
			curr_y = ((click_y - offset_y) / bounds.height) * height


			coords.push([parseInt(prev_x), parseInt(prev_y)])

			if (counter % 5 == 0) {
				strokes[strokes.length - 1][0].push(parseInt(prev_x))
				strokes[strokes.length - 1][1].push(parseInt(prev_y))
			}

			//console.log(ctx)
			ctx.beginPath()
			ctx.moveTo(prev_x, prev_y)
			ctx.lineTo(curr_x, curr_y)
			ctx.stroke()
			
			if (counter == 0){
				save()
			}
		}
		
		save = function() {

			//console.log(strokes)
			path = "images/" + curr_x + '-' + curr_y
			var image = canvas_raw.toDataURL("image/png")

			image = image.split(',')[1]

			if (strokes[strokes.length - 1][0].length == 0) {
				strokes.pop()
			}

			to_send = JSON.stringify(strokes)
			console.log(to_send)

			var xhr = new XMLHttpRequest()
			xhr.open('POST', 'http://127.0.0.1:8090/save_image/', true)
			xhr.setRequestHeader("X-CSRFTOKEN", csrftoken)


			xhr.onreadystatechange = function () {
				if (xhr.readyState == 4 && xhr.status == 200) {

					answer = $("#task").text()
					data = xhr.responseText

					console.log(data)
					var variants = data.replace("</li>", "").split('<li>')
					variants = variants.map(Function.prototype.call, String.prototype.trim)
					variants = variants.map(function(x) {return x.replace("</li>", "")}).slice(1)
					console.log(variants)
					console.log(answer)
					to_say = variants[0]
					console.log(last_said)
					if (variants.includes(answer)) {
						guess = 1
						to_say = "Oh, i know - it's " + answer
					} else {
						if (to_say != last_said  && false){

							if (first) {

								to_say = "maybe it's " + to_say
								first = false

							} else {
								to_say = "or " + to_say

							}
						}
					}
					if (to_say != last_said ) {
						last_said = to_say
						speak(to_say)
					}
					$('#variants > ul').html(data)
					console.log(guess)
					if (guess == 1){
						clear('')
						get_task()
					}
				}
			}
			xhr.send(JSON.stringify({data: strokes}))
		}


		start = function(event){

			strokes.push([[], []])
			moving = true
			draw(event, true)

		}
		
		stop = function(event){
			moving = false
			prev_x = 0;
			prev_y = 0;
			curr_x = 0;
			curr_y = 0;
			counter = 0
			save()
			
		}

		skip = function(event){
		    clear()
		    current_score -= 1
		    get_task()
		}

		move = function(event){
			if (moving) {
			
				draw(event, false)
				 
			}
			
		}
		
		speak = function(to_say){

			var msg = new SpeechSynthesisUtterance(to_say)
			msg.rate = 1.5
			msg.pitch = 1
			msg.lang = "en-US"
			speechSynthesis.speak(msg)

			//window.speechSynthesis.speak(msg)
			
		}
		
		clear = function(regime){

			if (regime == 'clear') {
				ctx.clearRect(0, 0, canvas_raw.width, canvas_raw.height)
			}
			else{
				console.log("transition!")
				canvas_transition(ImageData)
				//ctx.clearRect(0, 0, canvas_raw.width, canvas_raw.height)
			}

			strokes = []
			coords = []
			$('#variants > ul').html("")

			moving = false
			first = true
			prev_x = 0; prev_y = 0; curr_x = 0; curr_y = 0;
			counter = 0
			
		}

		canvas_transition = function(ImageData){

			var ImageData = canvas_raw.getContext("2d").createImageData(width, height)
			console.log(ImageData.data.buffer)
			offset = 10
			coords_stretched = coords.map(function(x){
				if (x[0] >= width / 2 && x[1] >= height / 2) {
					return [Math.max(x[0] + offset, width - offset), Math.min(x[1] + offset, height - offset)]
				}
				if (x[0] >= width / 2 && x[1] < height / 2){
					return [Math.min(x[0] + offset, width - offset), Math.max(x[1] - offset, 0)]
				}
				if (x[0] < width / 2 && x[1] >= height / 2){
					return [Math.max(x[0] - offset, 0), Math.min(x[1] + offset, height - offset)]
				}
				if (x[0] < width / 2 && x[1] < height / 2){
					return [Math.max(x[0] - offset, 0), Math.max(x[1] - offset, 0)]
				}
			})
			//console.log("length is " + coords_stretched.length)

			//console.log("index " + ImageData.data.indexOf(1))
			//ImageData.data.fill(1)
			console.log(ImageData.data)



			for(var coord in coords){
				//console.log(ImageData.data[((coord[0] * (ImageData.width * 4)) + (coord[1] * 4)) + 2])
				ImageData.data[((coord[0] * (ImageData.width * 4)) + (coord[1] * 4))] = 1
				//console.log(ImageData.data[((coord[0] * (ImageData.width * 4)) + (coord[1] * 4))])
				ImageData.data[((coord[0] * (ImageData.width * 4)) + (coord[1] * 4)) + 1] = 255
				ImageData.data[((coord[0] * (ImageData.width * 4)) + (coord[1] * 4)) + 2] = 0
				//ImageData.data[((coord[0] * (ImageData.width * 4)) + (coord[1] * 4)) + 3] = 1
			}

			console.log("value is " + ImageData.data[((coords_stretched[0][0] * (ImageData.width * 4)) + (coords_stretched[0][1] * 4)) + 2])

			ctx.putImageData(ImageData, 0, 0)
		}

		get_task = function(){

			current_score += 1
			score_display.textContent = current_score

			var xhr = new XMLHttpRequest()
			xhr.open('POST', 'http://127.0.0.1:8090/generate_task/', true)
			xhr.setRequestHeader("X-CSRFTOKEN", csrftoken)

			xhr.onreadystatechange = function()
			{
				if (xhr.readyState == 4 && xhr.status == 200)
				{
					data = xhr.responseText
					guess = 0
					console.log("data is" + data)
					$("#task").html(data)
				}
			};
			xhr.send(JSON.stringify({data: guess}))

		}

		start_stop_game = function(){
            var xhr = new XMLHttpRequest()

            xhr.open('POST', 'http://127.0.0.1:8090/save_leader/', true)
            xhr.setRequestHeader("X-CSRFTOKEN", csrftoken)

            xhr.onreadystatechange = function()
            {
                if (xhr.readyState == 4 && xhr.status == 200)
                {
                    data = xhr.responseText

                    data = data.split("|").slice(0,-1)

                    console.log("leaders!")
                    console.log(data)
                    $("#leaderboard > ul").html(data.map(function(x){
                        return "<li>" + x + "</li>"
                    }))
                }
            };

            var name_value = name_display.value == '' ? "Player" : name_display.value
            xhr.send(JSON.stringify({name: name_value, score: current_score}))
			if (!game_on) {
				start_stop_button.html("stop")
				startTimer(60, timer_display)
				canvas_raw.style.visibility = "visible"
				game_on = true
				get_task()
			}
			else{
				start_stop_button.html("start")
				clear('clear')
				current_score = -1
				$("#task").html("")
				score_display.textContent = ''
				stopTimer(timer_display)
				canvas_raw.style.visibility = "hidden"
				game_on = false
			}
		}
		canvas.on('mousedown', start)
		canvas.on('mouseup', stop)
		canvas.on('mousemove', move)
		canvas.on('mouseleave', stop)
        skip_button.on('click', skip)
		clear_button.on('click', clear)
		start_stop_button.on('click', start_stop_game)

	}
	
)
